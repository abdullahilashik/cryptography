from cryptography.fernet import Fernet


def encryption():
    key = Fernet.generate_key()

    with open('fernet.key','wb') as f:
        f.write(key)
    
    # read the image file ( content )
    with open('bangladesh_map.png','rb') as f:
        image_data = f.read()
    # encrypt the content
    fernet = Fernet(key)
    encrypted_data = fernet.encrypt(image_data)
    # save the encrypted new content
    with open('bangladesh_map.png','wb') as f:
        f.write(encrypted_data)
    print('File has been encrypted!')
    


def decryption():
    
    with open('fernet.key','rb') as f:
        key = f.read()
    
    # read the encrypted file
    with open('bangladesh_map.png','rb') as f:
        encrypted_data = f.read()
    # decrypt the content
    fernet = Fernet(key)
    decrypted_data = fernet.decrypt(encrypted_data)
    # write the decrypted content
    with open('bangladesh_map.png','wb') as f:
        f.write(decrypted_data)
    print('File has been decrypted!')


# encryption()
decryption()